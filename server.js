/*
 * @author Yariv Rosenbach
 * @repository https://github.com/yarivr/lp-labs-agent-sdk
 * Copyright 2016 LivePerson Inc - MIT license
 * https://github.com/yarivr/lp-labs-agent-sdk/LICENSE.md
 */
"use strict";

let AgentSDK = require('./index.js');

let BRANDID = "5477507"; // LiveEngage accountid
let USERNAME = "consumerBot"; // LiveEngage agent's login name
let PASSWORD = "Password1!"; // LiveEngage agent's password

let LAST_EVENT_TIMESTAMP = Date.now(); // The time stamp from which LiveEngage events should be fetched
let TRANSFER_SKILL_ID = -1; // Should be skill id (integer number) which the Bot should transfer a conversation to. -1 is equal to any skill
let General_Skill_ID = 17485313;
let CS_Skill_ID = 17509113;
let Sales_Skill_ID = 17509013;

let as = new AgentSDK(BRANDID, USERNAME, PASSWORD, Date.now());

as.on('consumer::ring', data => {
    as.acceptRing(data.ringId).then(() => {
        console.log(">>> ring accepted");
        as.getUserProfile(data.consumerId).then(
                data => { 
                  console.log(">>>Consumer Profile: ", data);
                  console.log("=================================================");
                  console.log("Consumer SDES : ");
                  console.log(JSON.stringify(data));
                  console.log("=================================================");
                });
                as.sendText(data.convId, "Hi, I'm Botty. How can I help you today?");
            }).catch((err) => {
        console.log(err.message);
    });
});

as.on('consumer::contentEvent', data => {
    console.log(">>>===GOT Next Message from consumer: ", data);
    checkOldMessages(data);
});

as.on('consumer::firstContentEvent', data => {
    console.log(">>>===GOT First Message from consumer: ", data);
    //checkMessage(data);
});

function checkOldMessages(tdata){
  var mainMessageSent = false;
  var tempMessages = as.queryMessages(tdata.convId,'10',tdata.sequence,'0');
  as.queryMessages(tdata.convId,10,tdata.sequence,0).then(
    data => { 
      // loop through the messages in the array to see if the main message has been sent yet
      for(var i = 0; i < data.length; i++){
        if(data[i].event.type == 'ContentEvent'){
          if(data[i].event.message == 'Is your inquiry sales relates, CS related, or about another topic?'){
            console.log("main message sent");
            mainMessageSent = true;
            break;
          }
        }
      }
      sendMessage(mainMessageSent,tdata);
    });
}

function sendMessage(recievedQuery,data){
  if(!recievedQuery){
    as.sendText(data.convId, "Is your inquiry sales relates, CS related, or about another topic?");
  }else{
    var tempMessage = data.message.toLowerCase();
    if (tempMessage.includes('transfer')) {
        as.transferToSkill(data.convId, General_Skill_ID);
    }else if(tempMessage.includes('cs')){
      as.sendText(data.convId, "Please wait while I transfer you.");
      setTimeout(function(){
        as.sendText(data.convId, "Transfering to 'CS Operations' now...");
        as.transferToSkill(data.convId, CS_Skill_ID);
      }, 1000);
    }else if(tempMessage.includes('sales')){
      as.sendText(data.convId, "Please wait while I transfer you.");
      setTimeout(function(){
        as.sendText(data.convId, "Transfering to 'Sales Operations' now...");
        as.transferToSkill(data.convId, Sales_Skill_ID);
      }, 1000);
    }else{
      as.sendText(data.convId, "Please wait while I transfer you.");
      setTimeout(function(){
        as.sendText(data.convId, "Transfering to 'General' now...");
        as.transferToSkill(data.convId, General_Skill_ID);
      }, 1000);
    }
  }
}